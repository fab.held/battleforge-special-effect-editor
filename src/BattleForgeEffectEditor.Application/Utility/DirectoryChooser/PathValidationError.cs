﻿// BattleForge Special Effect Editor
// Copyright(C) 2022 Skylords Reborn
// Project licensed under GNU General Public License v3.0. See LICENSE for more information.

namespace BattleForgeEffectEditor.Application.Utility
{
    public enum PathValidationError
    {
        Ok,
        DriveDoesNotExist,
        PathNotFound,
        NotADirectory,
        InvalidPath,
    }
}
