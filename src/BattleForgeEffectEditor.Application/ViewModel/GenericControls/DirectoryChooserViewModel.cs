﻿// BattleForge Special Effect Editor
// Copyright(C) 2022 Skylords Reborn
// Project licensed under GNU General Public License v3.0. See LICENSE for more information.

using BattleForgeEffectEditor.Application.Commands;
using BattleForgeEffectEditor.Application.Utility;
using System;
using System.Windows.Input;

namespace BattleForgeEffectEditor.Application.ViewModel.GenericControls
{
    public class DirectoryChooserViewModel : ObservableObject
    {
        public ICommand OpenOrSetDirectoryCommand => new RelayCommand((_) => OpenOrSetDirectory());

        public ICommand ClearDirectoryCommand => new RelayCommand((_) => ClearDirectory());

        public bool HasDirectorySet => !string.IsNullOrEmpty(Directory);

        public string Directory => GetDirectory();

        public Action<string> OnDirectorySet;
        public Action OnDirectoryCleared;
        public Func<string> GetDirectory;

        private DialogService dialogService = new DialogService();
        private WindowsService windowsServie = new WindowsService();

        public string SetOpenButtonText => HasDirectorySet ? "Open" : "Set";

        private void OpenOrSetDirectory()
        {
            if (HasDirectorySet)
                windowsServie.TryOpenExplorerOnDirectory(Directory);
            else
            {
                string path = Directory;
                if (string.IsNullOrEmpty(path))
                {
                    string dirPath = dialogService.OpenDirectoryDialog("Choose directory");
                    if (!string.IsNullOrEmpty(dirPath))
                        path = dirPath;
                }

                PathValidationError validationErr = PathValidator.ValidatePath(path);
                if (validationErr == PathValidationError.Ok)
                    SetDirectory(path);
                else
                    PathErrorThrow(validationErr);
            }
        }

        private void PathErrorThrow(PathValidationError pathValidatorError)
        {
            switch (pathValidatorError)
            {
                case PathValidationError.InvalidPath:
                    dialogService.ShowError("Invalid path", "No Path is set.");
                    break;
                case PathValidationError.DriveDoesNotExist:
                    dialogService.ShowError("Invalid path", "Selected Drive does not exist.");
                    break;
                case PathValidationError.PathNotFound:
                    dialogService.ShowError("Path not found", "Selected Path not found.");
                    break;
                case PathValidationError.NotADirectory:
                    dialogService.ShowError("Invalid path", "Selected Path is not a Directory.");
                    break;
                default:
                    break;
            }
        }

        private void SetDirectory(string dirPath)
        {
            OnDirectorySet?.Invoke(dirPath);
            RaiseAllPropertiesChanged();
        }

        private void ClearDirectory()
        {
            OnDirectoryCleared?.Invoke();
            RaiseAllPropertiesChanged();
        }
    }
}
